// Package routes defines actions store http router.
package routes

import (
	"encoding/json"
	"github.com/crackcomm/actions-store/models"
	"github.com/crackcomm/go-actions/action"
	"github.com/julienschmidt/httprouter"
	"io"
	"labix.org/v2/mgo/bson"
	"net/http"
	"strings"
)

var paramName = "name"

// Router - Actions store router.
var Router = httprouter.New()

// success body
var success = []byte(`{"success":true}`)

// InsertAction - Creates or updates an action
func InsertAction(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get action name
	project := ps.ByName("project")
	user := ps.ByName("user")
	name := NameParam(ps)

	// Read action from request body
	a := &action.Action{}
	err := json.NewDecoder(r.Body).Decode(a)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Insert (or replace) action
	err = models.InsertAction(user, project, name, a)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Write json encoded action
	err = WriteRequestedJSON(a, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// GetActions - Returns all actions found for user in project.
func GetActions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get request params
	project := ps.ByName("project")
	user := ps.ByName("user")
	name := NameParam(ps)

	// Get all actions
	actions, err := models.GetActions(user, project, name)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	// Write json encoded actions
	err = WriteRequestedJSON(actions, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// GetAction - Returns action found for user in project by name.
func GetAction(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get project & user params
	project := ps.ByName("project")
	user := ps.ByName("user")
	name := NameParam(ps)

	// Find action
	a, err := models.GetAction(user, project, name)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	// Write json encoded action
	err = WriteRequestedJSON(a, r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// DeleteAction - Deletes action(/s) matching selector (project and user are required, name is optional).
func DeleteAction(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get request parameters
	project := ps.ByName("project")
	name := NameParam(ps)
	user := ps.ByName("user")

	// Delete all actions matching selector
	err := models.DeleteAction(user, project, name)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	// Write status ok
	w.Write(success)
}

// DeleteActions - Deletes action(/s) matching selector (project and user are required, name is optional).
func DeleteActions(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Get request parameters
	project := ps.ByName("project")
	user := ps.ByName("user")
	name := NameParam(ps)

	// Delete all actions matching selector
	err := models.DeleteActions(user, project, name)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	// Write status ok
	w.Write(success)
}

// HealthCheck - Returns status 200.
func HealthCheck(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.Write(success)
}

// NameParam - Gets `name` parameter, replaces `/` to `.` and returns.
func NameParam(ps httprouter.Params) string {
	name := ps.ByName("name")
	name = strings.Trim(name, "/")
	return strings.Replace(name, "/", ".", -1)
}

// WriteRequestedJSON - Writes pretty json or raw json if `?raw` parameter in URL.
func WriteRequestedJSON(v interface{}, r *http.Request, w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")

	// Check `raw` parameter existence
	if _, raw := r.URL.Query()["raw"]; raw {
		return WriteJSON(v, w)
	}

	return WritePrettyJSON(v, w)
}

// WriteJSON - Writes JSON.
func WriteJSON(v interface{}, w io.Writer) error {
	return json.NewEncoder(w).Encode(v)
}

// WritePrettyJSON - Writes pretty JSON.
func WritePrettyJSON(v interface{}, w io.Writer) error {
	// Marshal JSON
	body, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return err
	}
	// Write JSON
	w.Write(body)
	return nil
}

// NameToID - Converts name to ObjectId if its one.
// If `create` is true and name is empty it creates new id and `created` is true.
func NameToID(name interface{}, create bool) (id interface{}, created bool) {
	// If create is true and name is empty - create new id
	if create {
		// If name is nil or empty string
		if n, ok := name.(string); name == nil || ok && n == "" {
			id = bson.NewObjectId()
			created = true
			return
		}
	}

	// Try to cast name to ObjectId
	var ok bool
	if id, ok = name.(bson.ObjectId); ok {
		return
	}

	// By default id is a name
	id = name

	// If name is ObjectIdHex convert it
	if n, ok := name.(string); ok && bson.IsObjectIdHex(n) {
		id = bson.ObjectIdHex(n)
	}

	return
}

func init() {
	Router.GET("/api/v1/:user/:project/actions", GetActions)
	Router.DELETE("/api/v1/:user/:project/actions", DeleteActions)
	Router.POST("/api/v1/:user/:project/actions/*name", InsertAction)
	Router.GET("/api/v1/:user/:project/actions/*name", GetAction)
	Router.PUT("/api/v1/:user/:project/actions/*name", InsertAction)
	Router.DELETE("/api/v1/:user/:project/actions/*name", DeleteAction)

	// Health check
	Router.GET("/health", HealthCheck)
}
