# Actions store

Action store is a HTTP service which allows users to store and access actions and tests with low-latency & high-availablity.

Actions are tested before every update, that's why tests should be always updated first.

## Roadmap

* Pluggable interfaces
	* Redis

## Storage

Actions store uses mongodb to store actions.

## Usage

To set mongodb url you can use `mongo_url` environment variable or `-database` flag.

	$ actions-store -help
	Usage of actions-store:
	  -address=":3000": HTTP listening address
	  -database="": MongoDB URL
	$ actions-store -database=mongodb://user:pass@db.mongolab.com:21700/database
	[actions-store] listening on :3000

## Code

Action store is written in [Go](http://golang.org).

## REST API v1

	// collection
	POST    /api/v1/:user/:project/actions         Insert actions (todo)
	GET     /api/v1/:user/:project/actions         Get actions
	DELETE  /api/v1/:user/:project/actions         Delete actions

	// action
	POST    /api/v1/:user/:project/actions/*name   Insert action
	PUT     /api/v1/:user/:project/actions/*name   Update action
	GET     /api/v1/:user/:project/actions/*name   Get action
	DELETE  /api/v1/:user/:project/actions/*name   Delete action

	GET     /health                                Health Check

## Communication

* Possible input/output formats: json
* Possible communication encoding: gzip.
* Possible communication protocols: HTTP. (TLS soon).

## Testing

Tests for v1 are in `tests/v1` and can be runned using [action-test](#).

```sh
$ cmds test
# or directly
$ action-test -tests=tests/v1/tests.json
```
