package main

import (
	"flag"
	"github.com/codegangsta/negroni"
	"github.com/crackcomm/actions-store/models"
	"github.com/crackcomm/actions-store/routes"
	"github.com/crackcomm/actions-store/rpc"
	"github.com/phyber/negroni-gzip/gzip"
	"log"
	"net/http"
	"os"
)

var (
	cors       string // Cross-origin resource sharing rule.
	address    string // HTTP listening address.
	rpcaddress string // RPC listening address.
	database   string // MongoDB URL.
)

// parseFlags - Returns address flag and database url flag or mongo_url env variable.
func parseFlags() {
	// Parse Cross-origin resource sharing rule.
	flag.StringVar(&cors, "cors", "*", "Cross-origin resource sharing rule.")
	// Parse MongoDB url flag
	flag.StringVar(&database, "database", "", "MongoDB URL (env - mongo_url)")
	// HTTP Listening address flag
	flag.StringVar(&address, "address", ":3000", "HTTP listening address")
	// RPC Listening address flag
	flag.StringVar(&rpcaddress, "rpc", ":3030", "JSON-RPC listening address")
	flag.Parse()

	// If database url is empty get it from env variable
	if database == "" {
		database = os.Getenv("mongo_url")
	}

	return
}

// corsMiddleware - Cross-origin resource sharing middleware.
func corsMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	// Set cors if origin is set
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Add("Access-Control-Allow-Origin", cors)
	}
	next(w, r)
}

func main() {
	// Parse flags
	parseFlags()

	// New logger
	l := log.New(os.Stdout, "[actions-store] ", 0)

	// If database url is empty its a fatal error
	if database == "" {
		l.Fatal("Database url can not be empty. Use `-database` flag or `MONGO_URL` environment variable.")
	}

	// Set database url
	err := models.SetDatabase(database)
	if err != nil {
		l.Fatal(err)
	}

	// Todo: authentication
	n := negroni.Classic()
	n.Use(negroni.HandlerFunc(corsMiddleware))
	n.Use(gzip.Gzip(gzip.DefaultCompression))

	// Bind actions REST api
	n.UseHandler(routes.Router)

	// Start JSON-RPC server
	go func() {
		l.Printf("rpc listening on %s", rpcaddress)
		err := rpc.Listen(rpcaddress)
		if err != nil {
			panic(err)
		}
	}()

	// Start HTTP server
	l.Printf("listening on %s", address)
	l.Fatal(http.ListenAndServe(address, n))
}
