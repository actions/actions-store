package models

import (
	"fmt"
	"github.com/crackcomm/go-actions/action"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"strings"
)

// Session - MongoDB session.
var Session *mgo.Session

// ActionsCollection - Name of actions collection.
var ActionsCollection = "actions"

// StoreAction - Structure used to store action.
type StoreAction struct {
	ID      bson.ObjectId  `bson:"_id,omitempty"`
	Name    string         `bson:"name,omitempty"`
	User    string         `bson:"user,omitempty"`
	Project string         `bson:"project,omitempty"`
	Action  *action.Action `bson:"action,omitempty"`
}

// storeActions - Store actions list.
type storeActions []*StoreAction

// StringID - Returns action string Id.
func (a *StoreAction) StringID() string {
	if a.Name != "" {
		return a.Name
	}
	return a.ID.Hex()
}

// Add - Adds action from database.
func (a *StoreAction) Add() (err error) {
	// Get database session
	sess := Session.Copy()
	defer sess.Close()

	// Get database connection
	actions := sess.DB("").C(ActionsCollection)

	// Create query selector without action
	var q *StoreAction
	if a.Action != nil {
		q = &StoreAction{
			Project: a.Project,
			User:    a.User,
			Name:    a.Name,
		}
	} else {
		q = a
	}

	// Find action if any
	if err := actions.Find(q).One(q); err == nil {
		a.ID = q.ID
	} else {
		a.ID = bson.NewObjectId()
	}

	// Upsert action - insert or replace
	_, err = actions.UpsertId(a.ID, a)
	return
}

// Get - Gets action from database.
func (a *StoreAction) Get() (err error) {
	// Get database session
	sess := Session.Copy()
	defer sess.Close()

	// Get database connection
	actions := sess.DB("").C(ActionsCollection)

	// Find action
	err = actions.Find(a).One(a)
	return
}

// Remove - Removes action from database.
func (a *StoreAction) Remove() (err error) {
	// Get database session
	sess := Session.Copy()
	defer sess.Close()

	// Get database connection
	actions := sess.DB("").C(ActionsCollection)

	// Create query selector without action
	var q *StoreAction
	if a.Action != nil {
		q = &StoreAction{
			Project: a.Project,
			User:    a.User,
			Name:    a.Name,
			ID:      a.ID,
		}
	} else {
		q = a
	}

	// Remove action
	err = actions.Remove(q)
	return
}

// ActionsMap - Returns a map of actions.
func (actions storeActions) ActionsMap() (result action.Map) {
	result = action.Map{}
	for _, a := range actions {
		// Pass if action is nil
		if a == nil {
			continue
		}

		// Add action to map
		result[a.StringID()] = a.Action
	}
	return
}

// SetDatabase - Initializes connection to specified database.
func SetDatabase(database string) (err error) {
	// Establish connection with database
	Session, err = mgo.Dial(database)
	if err != nil {
		return
	}
	return
}

// GetActions - Gets all actions matching selector.
// Parameter `name` is a list for convienience, it can be empty. It will be joined to one name with dots.
func GetActions(user, project string, name ...string) (actions action.Map, err error) {
	// Make actions selector
	selector := actionsSelector(user, project, name...)

	// Get database session
	sess := Session.Copy()
	defer sess.Close()

	// Create actions query
	query := sess.DB("").C(ActionsCollection).Find(selector)

	// Get all actions
	result := storeActions{}
	err = query.All(&result)
	if err != nil {
		return
	}

	// Get actions map
	actions = result.ActionsMap()
	return
}

// GetAction - Gets one action matching selector.
func GetAction(user, project, name string) (a *action.Action, err error) {
	// Make action selector
	response := &StoreAction{
		Project: project,
		User:    user,
		Name:    name,
	}

	// Get action from database
	err = response.Get()
	if err != nil {
		return
	}

	// Success
	a = response.Action
	return
}

// InsertAction - Inserts action to database (replaces if exists).
func InsertAction(user, project, name string, a *action.Action) (err error) {
	// Make action selector
	ex := &StoreAction{
		Project: project,
		User:    user,
		Name:    name,
		Action:  a,
	}

	// Insert action
	err = ex.Add()
	return
}

// DeleteActions - Deletes all actions matching selector.
// Parameter `name` is a list for convienience, it can be empty. It will be joined to one name with dots.
func DeleteActions(user, project string, name ...string) (err error) {
	// Make actions selector
	selector := actionsSelector(user, project, name...)

	// Get database session
	sess := Session.Copy()
	defer sess.Close()

	// Get database connection
	actions := sess.DB("").C(ActionsCollection)

	// Remove all actions matching selector
	_, err = actions.RemoveAll(selector)
	return
}

// DeleteAction - Deletes actions matching selector.
func DeleteAction(user, project, name string) (err error) {
	// Make actions selector
	selector := &StoreAction{
		Project: project,
		User:    user,
		Name:    name,
	}

	// Remove action matching selector
	err = selector.Remove()
	return
}

func actionsSelector(user, project string, name ...string) bson.M {
	selector := bson.M{
		"project": project,
		"user":    user,
	}

	// Make selector matching `{name}.actions` and `{name}` but not `{name}more`
	if len(name) >= 1 {
		selector["name"] = bson.M{
			"$options": "i",
			"$regex":   fmt.Sprintf("%s\\.?.*", strings.Join(name, ".")), // "test" name -> matches eq. test.fnc
		}
	}

	return selector
}
