package rpc

import (
	"github.com/crackcomm/actions-store/models"
	"github.com/crackcomm/go-actions/action"
	"net"
	r "net/rpc"
	"net/rpc/jsonrpc"
)

// Actions - Actions store rpc handler.
type Actions int

// Get - Gets action.
func (_ *Actions) Get(query *models.StoreAction, result *action.Action) (err error) {
	// Set Action
	query.Action = result

	// Get action from database
	err = query.Get()
	return
}

// Add - Adds action.
func (_ *Actions) Add(query *models.StoreAction, ok *bool) (err error) {
	// Add action to database
	err = query.Add()
	if err == nil {
		*ok = true
	}
	return
}

// Delete - Deletes action.
func (_ *Actions) Delete(query *models.StoreAction, ok *bool) (err error) {
	// Delete action to database
	err = models.DeleteActions(query.User, query.Project, query.Name)
	if err == nil {
		*ok = true
	}
	return
}

// Listen - Starts listening for JSON-RPC.
func Listen(address string) (err error) {
	// Start a listener on `address`
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return
	}
	defer listener.Close()

	// Listening for connections
	for {
		if conn, err := listener.Accept(); err == nil {
			go jsonrpc.ServeConn(conn)
		}
	}
}

// init - Registers RPC methods.
func init() {
	r.Register(new(Actions))
}
